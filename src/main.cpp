//============================================================================
// Name        : happy-piano.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <sstream>

#include <piano.hpp>

using namespace std;

int main(int argc, char* argv[]) {
  if(argc > 1) {
    HappyPiano::Piano().play_file( argv[1] );
  }
  else {
    HappyPiano::Piano().play_string("do do do fa la _ do do do fa la _ fa fa mi mi re re do _ do do do mi sol _ do do do mi sol _ do re do si la sol fa");
  }

	return 0;
}
