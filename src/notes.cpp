/*
 * notes.cpp
 *
 *  Created on: 30/04/2015
 *      Author: Dani
 */

#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>

#include <windows.h>
#include <notes.hpp>

using namespace HappyPiano;
using namespace std;

Note::Note(string data) {
  stringstream(data) >> m_name >> m_frequency >> m_duration;
}

Note::Note(string name, int frequency, int duration) {
  m_name = name;
  m_frequency = frequency;
  m_duration = duration;
}

void Note::play(float scale) {
  Beep(m_frequency, round(m_duration * scale) );
  cout << m_name << " " << flush;
}





bool Notes::s_initialized = false;
notes_map_t Notes::s_notes;

void Notes::initialize(string note_file) {
  if(s_initialized) return;

  ifstream is(note_file);

  for (std::string line; std::getline(is, line); ) {
    // Limpiar espacios sobrantes
    line.erase(0, line.find_first_not_of( " \t\n\r\f\v" ));

    // Si est� vac�a o empieza por "#", ignorar.
    if(line.length() == 0 or line[0] == '#') continue;

    // Leer datos
    Note note(line);

    // Y a�adir la nota creada
    s_notes.insert(std::pair<std::string, Note>(note.get_name(), note) );
  }

  s_initialized = true;
}

Note Notes::get_note(string note_name) {
  initialize();

  map<string, Note>::iterator it = s_notes.find( note_name );
  if(it == s_notes.end())
    throw Exception("Invalid note name " + note_name + ": not found.");

  return it->second;
}
