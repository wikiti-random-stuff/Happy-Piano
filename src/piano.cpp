/*
 * piano.cpp
 *
 *  Created on: 30/04/2015
 *      Author: Dani
 */

#include <fstream>
#include <sstream>

#include <piano.hpp>

using namespace std;
using namespace HappyPiano;

Piano::Piano(float scale) {
  m_scale = scale;
}

void Piano::play_file(std::string file) {
  ifstream is(file);

  for(string line; getline(is, line); ) {
    play_string( line );
  }
}

void Piano::play_string(std::string melody) {
  stringstream ss(melody);

  for(string note; ss >> note; )
    HappyPiano::Notes::get_note(note).play(m_scale);
}

