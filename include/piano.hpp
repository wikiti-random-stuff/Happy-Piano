/*
 * piano.hpp
 *
 *  Created on: 30/04/2015
 *      Author: Dani
 */

#ifndef PIANO_HPP_
#define PIANO_HPP_

#include <string>
#include <vector>

#include <notes.hpp>

namespace HappyPiano {
  class Piano {
    private:
      float m_scale;

    public:
      Piano(float scale = 1.f);

      void play_file(std::string file);
      void play_string(std::string melody);
  };
}




#endif /* PIANO_HPP_ */
