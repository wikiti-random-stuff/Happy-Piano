/*
 * notes.hpp
 *
 *  Created on: 30/04/2015
 *      Author: Dani
 */

#ifndef NOTES_HPP_
#define NOTES_HPP_

#include <exception>
#include <map>
#include <string>

#define DEFAULT_NOTES_FILE "notes.hp"

namespace HappyPiano {
  class Exception : public std::exception {
    private:
      std::string m_msg;

    public:
      Exception(std::string text = ""): m_msg(text) { }

      const char* what() const throw () {
        return m_msg.c_str();
      }
  };

  class Note {
    private:
      std::string m_name;

      int m_frequency;
      int m_duration;

    public:
      Note(std::string data);
      Note(std::string name, int frequency, int duration);

      std::string get_name() { return m_name; }

      void play(float scale = 1.0);
  };

  typedef std::map<std::string, Note> notes_map_t;

  class Notes {
    private:
      static bool s_initialized;
      static notes_map_t s_notes;

      static void initialize(std::string note_file = DEFAULT_NOTES_FILE);

    public:
      static Note get_note(std::string note_name);
  };
}


#endif /* NOTES_HPP_ */
