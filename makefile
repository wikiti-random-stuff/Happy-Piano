CC=g++
CFLAGS=-c -Wall -Iinclude --std=c++11
LDFLAGS=
SOURCES=src/main.cpp src/piano.cpp src/notes.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=bin/happy_piano

all: $(SOURCES) $(EXECUTABLE)
    
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
