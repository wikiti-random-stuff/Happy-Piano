# Happy piano #

![](assets/hp.png)

## 1. Summary ##

Happy piano is a simple piano program to play simple melodies with windows "beeps".

## 2. Dependencias ##
It's necesary to compile and execute this program in Windows OS to make it work.

## 3. Compiler ##
Recomended to use [**Mingw**](http://www.mingw.org/) with *C++11*.
***C++*** implementado en  (principalmente), *GCC* o cualquier compilador libre de *C++*

## 4. Use ##

First, compile it by using the provided *makefile*:

````sh
$ make
````

Then, you can execute this program by moving into the *bin* directory and then executing this:

````sh
$ happy_piano <song_file>
````

You can also play a file by dragging and dropping it into the executable.

## 5. Configuration ##

The notes can be viewed and modified in the *notes.hp* file. Each line has the following format:

````
# This line is a comment.
# The following line is a blank line.

# Note, each note has the following properties:
# name     frequency    duration
do         261          130
````

Also, melody files o *<songs>.hpm* has the following format:

````
note1 note2 note3
note2 note2 note3

note1 note2 note3
````

For example, try this:

````
do do do fa la _ _
do do do fa la _ _
fa fa mi mi re re do _ _

do do do mi sol _ _
do do do mi sol _ _
do re do si la sol fa
````

## 5. Authors ##

This program has been developed by:

<!-- Tabla -->
<table cellspacing="0">
  <tr  style="background-color: #E3E3E3;">
    <td> <b>Avatar</b> </td>
    <td> <b>Nane</b> </td>
    <td> <b>Nickname</b> </td>
	<td> <b>Email</b> </td>
  </tr>
  <tr style="background-color: #FFFFFF;">
    <td> <img width="64"src="http://imageshack.us/a/img209/6782/parrotav.png"/> </td>
    <td> Daniel Herzog Cruz </td>
    <td> <b>Wikiti</b> </td>
	<td> <a href="mailto:wikiti.doghound@gmail.com"> wikiti.doghound@gmail.com</a> </td>
  </tr>
</table>
<!-- Fin tabla -->